const fs = require('fs')
const path = require("path");

const makeDirectory=(dirName,callback)=>{
    fs.mkdir(path.join(__dirname,dirName),(err)=>{
        if (err) {
            return console.error(err);
        }
        else{
        console.log('Directory created successfully!');}
        callback()
    })
}

const makeRandomFiles=(filePath,data,callback)=>{
    fs.writeFile(path.join(filePath,""),data,(err=>{
        if (err) {
            return console.error(err);
        }
        else{
        console.log('file created successfully!');}
        callback()
        
    }))

}


const  deleteFiles=(filePath, callBack) =>{
    fs.unlink(path.join(filePath, ''), (err) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log('Json File Deleted')
            callBack()
        }
    })
};


module.exports={makeDirectory, makeRandomFiles,deleteFiles}