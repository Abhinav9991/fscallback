const fs = require("fs")
const path = require("path")

const readFile = (filePath, callback) => {
    fs.readFile(path.join(filePath, ''), (err, data) => {
        if (err) {
            return console.error(err)
        }
        else {
            console.log(`${path.basename(filePath)} is read successfuly`)
            callback(data.toString())
        }
    })

}
const writeTOfile = (data, filePath, callback) => {
    fs.writeFile(path.join(filePath, ''), data, (err) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log(`File Created  Succesfully at ${filePath}`)
            callback(filePath)
        }
    })

}

const filename = (data, filePath, callback) => {
    fs.appendFile(path.join(filePath, ''), path.basename(data) + '\n', (err) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log(`File updated  Succesfully at ${filePath}`)
            callback(filePath)
        }
    })

}

const appendFile = (data, filePath, callback) => {
    fs.appendFile(path.join(filePath, ''), path.basename(data) + '\n', (err) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log(`File updated Successfully`)
            callback(data)
        }
    })
}

const deleteFiles = (filePath) => {
    fs.unlink(path.join(filePath, ''), (err) => {
        if (err) {
            console.log(err.message)
        }
        else {
            console.log('File Deleted')

        }
    })
};




module.exports = { readFile, writeTOfile, filename, appendFile, deleteFiles }