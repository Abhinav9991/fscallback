const { readFile, writeTOfile, filename, appendFile, deleteFiles} = require("../problem2")

readFile("../lipsum.txt", (data) => {
   writeTOfile(data.toUpperCase(), "../UppercaseLipsum.txt", (filePath) => {
      filename(filePath, "../filenames.txt", () => {
         readFile(filePath, (data) => {
            let sentences = ""
            const lowerCaseData = data.toLowerCase().trim().split(".")
            lowerCaseData.map((eachSentc) => {
               sentences += eachSentc.trim() + "\n"
            })
            writeTOfile(sentences, "../lowerCaseSentenceFile.txt", (filePath) => {
               appendFile(filePath, '../filenames.txt', (filePath) => {
                  readFile(filePath, (data) => {
                     appendFile(data, "../newFile.txt", () => {
                        readFile("../newFile.txt", (data) => {
                           const sortedData = data.toString().trim().split("\n").sort()
                           let sentence = ''
                           sortedData.map((each) => {
                              if (each !== '') {
                                 sentence += each + "\n"
                              }
                           })
                           writeTOfile(sentence, "../newFile.txt", (filePath) => {
                              filename(filePath, "../filenames.txt", () => {
                                 readFile("../filenames.txt", (data) => {
                                    const filePath = data.trim().split("\n")
                                    filePath.map((eachPath) => {
                                       deleteFiles(`../${eachPath}`)
                                    })

                                 })
                              })
                           })
                        })
                     })
                  })
               })

            })
         })
      })
   })
})